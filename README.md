# desafio talana backend dev

### Observaciones:
La API se lanzará en el puerto 8000.

Ni la he probado, mientras tanto puede leer el código
El sorteo permite que existan varios registros de sorteos históricos (sino pa qué una app)

Los participantes siempre participan en el concurso "actual". De ese mismo modo, cuando se llame al endpoint para obtener un ganador, se generará un ganador aleatorio para el concurso actual.

Un concurso podría tener varios ganadores.


### Configurar variables de entorno
Para ejecutar la aplicación, deberá tener un archivo .env en la raiz del proyecto.
Debe declarar los siguientes valores:

Ejemplo archivo .env (no versionado, eso no se hace)
```
SECRET_KEY=<una llave string mas o menos grande (un salt para las contraseñas)>
DEBUG=<True o False>
ALLOWED_HOSTS=< una lista con los host donde va la app. Ej: localhost,127.0.0.1 >
DB_NAME=<el nombre de la db en el docker-compose.yml>
DB_USER=<el nombre del usuario de la db en el docker-compose.yml>
DB_PASSWORD=<la pass del usuario de la db en el docker-compose.yml>
DB_HOST=<el nombre del servicio de la db en el docker-compose.yml>
DB_PORT=<el puerto del servicio de la db en el docker-compose.yml (el que va antes de los dos puntos o explota)>

BASE_URL=http://localhost:8000 <en produccion debería llevar el nombre de su propia url, para los mail>

EMAIL_HOST=<el host del servidor de correos que va a usar el compa tipo smtp.mailgun.com>
EMAIL_USE_TLS=<True o False>
EMAIL_PORT=<el puerto del correo>
EMAIL_HOST_USER=<un correo>
EMAIL_HOST_PASSWORD=<la clave del correo>
SEND_EMAIL_ENABLED=<True o False si quieres que salgan correos o no>
```

### Instalar dependencias:
* sudo apt install docker
* sudo apt install docker-compose

Al ejecutar el comando build se instalará el resto en un contenedor (redis, python, postgres)

Redis es utilizado como repositorio para las tareas que serán invocadas de forma asíncrona por celery.


## Iniciar aplicación
> sudo docker-compose build

> sudo docker-compose run web python manage.py makemigrations

> sudo docker-compose run web python manage.py migrate

Para iniciar la api:
> sudo docker-compose up

Para iniciar la api en segundo plano:
> sudo docker-compose up -d
## Endpoints

* /api/v1/contest
    Dependiendo el método por el que viaje el request podrá crear (POST), actualizar (PUT), obtener (GET), eliminar (DELETE)

* /api/v1/account-validation?validation_code=<uuid:validation_code>
    Si existe un registro en el modelo CompetitorValidationRequest con ese validation_code, le va a asignar un usuario y va a setear una contraseña random que después el usuario podrá modificar en una interfaz de usuario. 
    
* /api/v1/new-contest-winner/
    Va a generar un nuevo ganador para el concurso actual
    
