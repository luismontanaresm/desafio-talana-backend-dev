import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'desafio_talana.settings')

app = Celery('desafio_talana')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
