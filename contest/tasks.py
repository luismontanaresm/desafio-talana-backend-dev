from desafio_talana.celery import app as celery_app
from django.core.mail import send_mail
from contest.models import (Competitor, CompetitorValidationRequest)
from contest import constants
from django.urls import reverse
from django.conf import settings

@celery_app.task(name='contest.send_account_validation', ignore_result=True)
def send_account_validation_email(competitor: Competitor):
    validation_request = CompetitorValidationRequest.new_competitor_validation_request(competitor=competitor)
    validation_link = validation_request.validation_link
    subject = constants.VERIFICATION_SUBJECT
    message = constants.VERIFICATION_MESSAGE_BODY.format(validation_link=validation_link)
    html_message = constants.VERIFICATION_MESSAGE_HTML.format(validation_link=validation_link)
    sender_name = settings.EMAIL_HOST_USER
    competitor_email = competitor.email
    if settings.SEND_EMAIL_ENABLED:
        send_mail(
            subject, message=message, from_email=sender_name, 
            recipient_list=[competitor_email],html_message=html_message)
    
