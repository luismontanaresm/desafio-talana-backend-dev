from django.shortcuts import render, redirect
from rest_framework import generics, viewsets
from rest_framework.views import APIView
from django.core.mail import send_mail
from contest import models as contest_models
from contest import serializers as contest_serializers
from contest import tasks as contest_tasks
from contest import constants
from django.http import HttpResponse, JsonResponse

class ContestAPIView(viewsets.ModelViewSet):
    model = contest_models.Contest
    serializer_class = contest_serializers.ContestSerializer
    queryset = contest_models.Contest.get_all_contests()

class CompetitorRegistrationAPIView(APIView):
    def post(self, *args, **kwargs):
        payload = self.request.POST
        name = payload.get('name')
        email = payload.get('email')
        phone = payload.get('phone')
        dni = payload.get('dni')
        response_message = ''
        email_is_used = contest_models.Competitor.check_competitor_is_validated_by_email(email)
        if email_is_used:
            response_message = constants.EMAIL_IS_USED.format(email=email)
        else:
            competitor = contest_models.Competitor.new_competitor(
                name=name, 
                email=email, 
                phone=phone, 
                dni=dni
                )
            response_message = constants.VERIFICATION_EMAIL_SENT.format(email=email)
            contest_tasks.send_account_validation_email.delay(competitor)
        response = HttpResponse(response_message)
        return response

class CompetitorValidationAPIView(APIView):
    def get(self, *args, **kwargs):
        validation_code = self.request.GET.get('validation_code')
        competitor, error_message = contest_models.CompetitorValidationRequest.validate_competitor_by_validation_code(validation_code)
        competitor.set_user_with_random_password()
        if error_message:
            response = HttpResponse(error_message)
            return response
        
        # TODO: Redirect to a user interface where user will set its password in a "set password" user interface .
        # No la haré, hay que dormir.
        response = HttpResponse(constants.VERIFICATION_SUCCESS_MESSAGE)
        return response

class NewContestWinnerAPIView(APIView):
    def post(self, *args, **kwargs):
        current_contest = contest_models.Contest.get_current_contest()
        new_winner = current_contest.new_contest_winner()
        new_winner_serializer = contest_serializers.CompetitorSerializer(new_winner)
        winner_data = new_winner_serializer.data
        return JsonResponse(winner_data)

