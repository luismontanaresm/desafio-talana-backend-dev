from rest_framework.serializers import ModelSerializer
from contest.models import (
    Contest,
    Competitor,
)

class ContestSerializer(ModelSerializer):
    class Meta:
        model = Contest
        fields = (
            'pk', 'created_at', 'updated_at', 'closed', 'winners'
            )
        depth = 1


class CompetitorSerializer(ModelSerializer):
    class Meta:
        model = Competitor
        fields = (
            'pk', 'created_at', 'updated_at', 
            'phone', 'dni', 'email', 'validation_timestamp'
            )
