EMAIL_IS_USED = """ La cuenta asociada al email {email} ya se encuentra registrada. """
VERIFICATION_EMAIL_SENT = """ Pronto te enviaremos un email de verificación al correo electrónico {email}. """
VERIFICATION_MESSAGE_BODY = """
Para completar tu registro y participes en nuestro nuevo concurso,
copia y pega el siguiente enlace en tu navegador: {validation_link}.
¡Mucha suerte!
"""
VERIFICATION_MESSAGE_HTML = """
    <p> Para completar tu registro y participes en nuestro nuevo concurso, haz click en el siguiente enlace </p>
    <br>
    <a href="{validation_link}"> Validar mi cuenta </a>.
    <p>Si no funciona, copia y pega el siguiente enlace en tu navegador: {validation_link}</p>
    <h3>¡Mucha suerte!</h3>
"""
VERIFICATION_SUBJECT = """ Verificación de cuenta concurso papel higiénico """
VERIFICATION_CODE_IS_EXPIRED = """ El código de verificación expiró. """
VERIFICATION_SUCCESS_MESSAGE = """ Su cuenta para participar en el concurso ha sido activada :) """