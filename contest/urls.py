from django.urls import path
from contest import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'contest', views.ContestAPIView)

urlpatterns = router.urls + [
    path(
        'competitor-registration', 
        views.CompetitorRegistrationAPIView.as_view(), 
        name='contest-competitor-registration-view'
        ),
    path(
        'account-validation',
        views.CompetitorValidationAPIView.as_view(),
        name='contest-competitor-validation-view'
        ),
    path(
        'new-contest-winner',
        views.NewContestWinnerAPIView.as_view(),
        name='contest-new-contest-winner-view'
        ),
]
