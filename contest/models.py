from django.db import models
from base.models import BaseModel
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from contest import constants
from uuid import uuid4
from django.utils import timezone
from django.conf import settings
from django.urls import reverse

class Contest(BaseModel):
    closed = models.BooleanField(verbose_name='Concurso terminado', default=False)

    @classmethod
    def get_all_contests(cls):
        all_contests = cls.objects.order_by('created_at').all()
        return all_contests
    
    @classmethod
    def get_current_contest(cls):
        current_contest = cls.objects.filter(closed=False).order_by('-created_at').first()
        return current_contest
    
    @property
    def winners(self):
        contest_winners = ContestWinner.objects.filter(contest=self).all()
        return contest_winners
    
    def new_contest_winner(self):
        validated_competitors_qs = Competitor.validated_competitors()
        new_winner = validated_competitors_qs.order_by('?').first()
        ContestWinner.objects.create(contest=self, winner=new_winner)
        return new_winner


class Competitor(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    validation_timestamp = models.DateTimeField(verbose_name='Fecha validación', null=True)
    name = models.CharField(verbose_name='Nombre')
    email = models.EmailField(verbose_name='Email')
    phone = models.CharField(verbose_name='Teléfono', max_length=12, blank=True)
    dni = models.CharField(verbose_name='Rut o Pasaporte', max_length=16, blank=True)

    @classmethod
    def validated_competitors(cls):
        objs = cls.objects.filter(validation_timestamp__isnull=False).all()
        return objs

    @classmethod
    def check_competitor_is_validated_by_email(cls, email:str):
        competitor_is_validated = (
            cls
            .objects
            .filter(email=email, validation_timestamp__isnull=False)
            .exists()
        )
        return competitor_is_validated
    
    @classmethod
    def new_competitor(cls, **kwargs):
        competitor_obj = cls.objects.create(**kwargs)
        return competitor_obj

    def set_validation_timestamp(self):
        now = timezone.utcnow()
        self.validation_timestamp = now
        self.save()

    def set_user_with_random_password(self):
        password = User.objects.make_random_password()
        email = self.email
        username = self.email
        user = User.objects.create_user(username, email, password)
        self.user = user
        self.save()
    

class CompetitorValidationRequest(BaseModel):
    competitor = models.ForeignKey(Competitor)
    validation_code = models.UUIDField(default=uuid4)
    
    @property
    def is_expired(self):
        competitor_validation_requests = self.get_competitor_validation_requests(competitor=self.competitor)
        latest_validation_request_for_competitor = competitor_validation_requests.order_by('created_at').last()
        expired = self != latest_validation_request_for_competitor
        return expired

    @classmethod
    def new_competitor_validation_request(cls, competitor: Competitor):
        comp_validation_obj = cls.objects.create(competitor=competitor)
        return comp_validation_obj
    
    @classmethod
    def get_competitor_validation_requests(cls, competitor: Competitor):
        comp_validation_objs = cls.objects.filter(competitor=competitor).all()
        return comp_validation_objs

    @classmethod
    def validate_competitor_by_validation_code(cls, validation_code: str):
        competitor, error_message = (None, None)
        validation_request = cls.objects.filter(validation_code=validation_code).first()
        if validation_request.is_expired:
            error_message = constants.VERIFICATION_CODE_IS_EXPIRED
        else:
            competitor = validation_request.competitor
            competitor.set_validation_timestamp()
        return competitor, error_message 

    @property
    def validation_link(self):
        link_squeleton = '{base_url}{endpoint}?validation_code={validation_code}'
        base_url = settings.BASE_URL
        endpoint = reverse('contest-competitor-validation-view')
        validation_code = self.validation_code
        link = link_squeleton.format(base_url=base_url, endpoint=endpoint, validation_code=validation_code)
        return link





class ContestWinner(BaseModel):
    contest = models.ForeignKey(verbose_name='Concurso', to=Contest, on_delete=models.CASCADE)
    winner = models.ForeignKey(verbose_name='Ganador', to=Competitor, on_delete=models.CASCADE)

